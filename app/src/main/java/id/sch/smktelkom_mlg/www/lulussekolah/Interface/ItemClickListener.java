package id.sch.smktelkom_mlg.www.lulussekolah.Interface;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
