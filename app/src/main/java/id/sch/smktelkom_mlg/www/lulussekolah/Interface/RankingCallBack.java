package id.sch.smktelkom_mlg.www.lulussekolah.Interface;

public interface RankingCallBack<T> {
    void callBack(T ranking);
}
